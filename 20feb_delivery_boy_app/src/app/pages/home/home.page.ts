import { Component } from '@angular/core';
import { NavController, MenuController, LoadingController, Platform } from '@ionic/angular';
import { TranslateProvider, HotelProvider } from '../../providers';
// import { CarsListPage } from './cars-list.page';
import { environment } from '../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../auth.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  openMenu: Boolean = false;
  searchQuery: String = '';
  items: string[];
  showItems: Boolean = false;
  rooms: any;
  adults: any;
  subscribe: any;
  childs: any = 0;
  children: number;
  hotellocation: string;

  agmStyles: any[] = environment.agmStyles;
  id
  token
  flour_type
  rootPage
  first_name
  last_name
  society_id
  email
  mobile
  subscription
  profile_photo
  delivery_boy
  List
  order_list
  status=1
  order_id
  order_status
  clicked = false;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private translate: TranslateProvider,
    public hotels: HotelProvider,
    private route: ActivatedRoute,
    private platform: Platform,
    private authservice: AuthService
  ) {

    this.token = localStorage.getItem("token");
    this.id = localStorage.getItem('id');

    this.authservice.order_list().subscribe((data)=>{
      this.List=data
      this.order_list=this.List.data
      console.log(this.order_list);
    

    //   for (let i = 0; i < this.order_list.length; i++) {
    //     this.order_id = this.order_list[i].order_id;
    //     console.log(this.order_id);
    //   }
    })




  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }


  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator['app'].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }



  ngOnInit() {


    if (localStorage.getItem("id") === null) {
      this.navCtrl.navigateRoot('/login');
    }

    // this.route.queryParams

    //   .subscribe(params => {
    //     console.log("params",params); // {order: "popular"}

    //     // this.order = params.order;
    //     // console.log(this.order); // popular
    //   });

  }

  logout() {
    this.navCtrl.navigateRoot('login');
  }

  register() {
    this.navCtrl.navigateForward('register');
  }

  view(id) {
    console.log(id)
    this.navCtrl.navigateForward('/grain-flour-list', {queryParams :{id:id }})
    console.log(id);
    
  } 

  details(order_id){
    this.navCtrl.navigateForward(`order-details/${order_id}`);
  }

  deliver(order_id){
      
  
    console.log("TCL: HotelCheckoutPage -> edit -> ComplaintList",order_id)
    this.order_id=order_id

    // this.navCtrl.navigateForward(`adddelivery/${order_id}`);
    this.authservice.order_status(this.order_id,this.status).subscribe((data)=>{

      this.order_status=data
  
      // this.order_list=this.List.data
      console.log(this.order_status);
    })
    this.order_id='disabled';

  }

}
